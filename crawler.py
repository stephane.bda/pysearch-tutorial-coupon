#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bs4
import requests
import time


def extract_href_from(urls=None, recrawl=None):
    """ Extract link from list of url if link contain some href tag """
    if isinstance(urls, str):
        urls = [urls]  # If one url in params : one URL list
    result = list()
    start = time.time()
    if recrawl:
        print("\n[+] Found multiple coupon and links in href, Recrawling them, please wait...\n")
    else:
        print("[+] urls submitting to crawler, searching href tag in links :")
    for link in urls:
        try:
            response = requests.get(link, timeout=10)
        except requests.exceptions.InvalidSchema:
            print("\n[+] requests.exceptions.InvalidSchema in url : " + link + "\n")
            continue
        except requests.exceptions.ConnectionError:
            print("\n[+] requests.exceptions.ConnectionError: in url : " + link + "\n")
            continue
        except requests.exceptions.MissingSchema:
            print("\n[+] requests.exceptions.MissingSchema: in url : " + link + "\n")
            continue
        except requests.exceptions.ReadTimeout:
            print("\n[+] requests.exceptions.ReadTimeout: in url : " + link + "\n")
            continue
        soup = bs4.BeautifulSoup(response.text, "html.parser")

        #  Find all href links
        for link_node in soup.find_all('a'):
            href = link_node.get('href')
            if href:  # Dont append None in result array if we want to sort it
                result.append(href)
        stop = time.time()
        print("%50s url done in %3.1fs" % (link, stop - start))
        start = stop

    return sorted(list(set(result)))  # Return only uniq links


def filter_exclude_url(urls, excludes):
    """ Exclude link from list of url if link contain some links_to_exclude """
    result = []
    for url in urls:
        for exclude in excludes:
            if url.startswith(exclude):
                break
        else:
            result.append(url)
    return result


def filter_wordtofind(urls, word_to_find):
    """ Filter link from word_to_find list if link contain some word_to_find """
    result = []
    for url in urls:
        for word in word_to_find:
            if word in url:
                result.append(url)
                break
    return sorted(list(set(result)))  # Return only uniq links


def filter_special_domain_with_word_to_find(links_to_run, links_special_domain):
    """ Filter for special domain name who contain word to find; example domain onehack.us """
    filtered_domain = []
    for elem in links_to_run:
        for domain in links_special_domain:
            if elem.startswith(domain):
                for word in word_to_find:
                    if word in elem.lstrip(domain) and elem not in filtered_domain:
                        filtered_domain.append(elem)

    return filtered_domain


def filter_links_with_multiple_coupon(links_to_run, word_multiple_coupon):
    """ Some links are multiple coupon in 1 links (example 9-in-1), recrawl them """
    multiple_coupon = []
    for link in links_to_run:
        for word in word_multiple_coupon:
            if word in link:
                multiple_coupon.append(link)
                break

    return sorted(list(set(multiple_coupon)))  # Return only uniq links


def merge_lists(filtered_domain, filtered_link_to_exclude, links3=None):
    """ Merge 2 list """
    filtered = set(filtered_domain + filtered_link_to_exclude)
    return sorted(filtered)


def recrawl_or_not(urls, link_to_search):
    result = []
    links = []
    for url in urls:
        for word in link_to_search:
            if word in url:
                result.append(url)
                break
            else:
                links.append(url)

    return result, links


def crawler(target_url, word_to_find, excludes_url):
    print("[+] Starting")
    #  Extract href
    links = extract_href_from(target_url)
    result, links = recrawl_or_not(links, link_to_search)
    # Search if multiple coupon in links:
    multiple_coupon = filter_links_with_multiple_coupon(links, word_multiple_coupon)
    # If multicoupon in links, recrawl it
    multiple_coupon = extract_href_from(multiple_coupon, recrawl=True)
    # Merge recrawl multi coupon & original list
    links = merge_lists(multiple_coupon, links, result)
    # Searching word in special domain
    filtered_domain = filter_special_domain_with_word_to_find(links, links_special_domain)
    # Exclude some links with special Domain
    links = filter_exclude_url(links, links_special_domain)
    # Merge
    links = merge_lists(filtered_domain, links)
    # We got all the links, now we filter them
    print("links total : %d, \n filtering using exclude URL : " % len(links))
    links = filter_exclude_url(links, excludes_url)
    print("links total : %d, \n filtering using word to find : " % len(links))
    links = filter_wordtofind(links, word_to_find)
    print("links total : %d \n " % len(links))
    return links


target_url = ['https://couponscorpion.com/',
              'https://freebiesglobal.com/',
              'https://onehack.us/tags/udemycoupons',
              'https://schoobly.com/',
              'https://tricksinfo.net/',
              'https://udemycoupon.learnviral.com/coupon-category/free100-discount/',
              'https://udemycoupons.me',
              'https://udemyfreebies.com/',
              'https://www.dealabs.com/bons-plans/udemy',
              'https://www.ilearne.com/',
              'https://www.onlinetutorials.org/category/100-off-udemy-coupon/',
              'https://www.promocoupons24.com/',
              'https://www.real.discount/',
              'https://www.udemy.com/courses/search/?src=ukw&q=free%20courses']
word_to_find = ['attack', 'attaque', 'backdoor', 'blue', 'bug', 'ceh', 'burp', 'comptia', 'crack', 'crawl', 'cyber',
                'defense', 'EDR', 'enginee', 'ethical', 'exploit', 'forensic', 'forensique', 'hack', 'hardening',
                'hidden', 'incident', 'inject', 'intrusion', 'keylog', 'linux', 'malware', 'metasploit', 'monitor',
                'network', 'overflow', 'owasp', 'pentest', 'pwa', 'red', 'réponse', 'response', 'reseau', 'remote',
                'revers', 'scanner', 'scapy', 'SIEM', 'scrapy', 'script', 'secur', 'server', 'shell', 'ssh', 'socket',
                'surveillance', 'vulnerab', 'incident', 'EBIOS', 'ISO', 'LPM', 'SSI', 'host', 'c2', 'curl', 'netcat',
                'rootkits']
links_to_exclude = ['http://www.promocoupons24.com/2017/',
                    'https://feedly.com/i/subscription/',
                    'https://freebiesglobal.com/category/',
                    'https://couponscorpion.com/recommends/hosting',
                    'https://onehack.us/tag/',
                    'https://onehack.us/tags/',
                    'https://tricksinfo.net/channel/',
                    'https://twitter.com',
                    'https://www.addtoany.com/add_to/reddit',
                    'https://www.real.discount/offer_cat/',
                    'https://www.real.discount/search-page/offer_tag/',
                    'https://www.ilearne.com/tag/',
                    'https://www.ilearne.com/engineering/',
                    'https://www.ilearne.com/growth-hacking/',
                    'https://www.ilearne.com/network-and-security/',
                    'https://www.ilearne.com/self-defense/',
                    'https://www.ilearne.com/software-engineering/',
                    'https://www.ilearne.com/operating-systems/',
                    'https://www.ilearne.com/operations/',
                    'https://www.ilearne.com/rede-e-seguranca/',
                    'https://www.ilearne.com/strategy/',
                    'https://www.facebook.com',
                    'https://reddit.com',
                    'https://stacksocial.com',
                    'https://pinterest.com',
                    'https://digg.com',
                    'javascript:void(0)'
                    ]
word_multiple_coupon = ['-in-1']
links_special_domain = ['https://onehack.us/']
link_to_search = ['https://www.udemy.com',
                  'https://www.eduonix.com/']

result = crawler(target_url, word_to_find, links_to_exclude)
for url in result:
    print(url)

