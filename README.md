# PySearch tutorial coupon

## Python script who crawl multiple website and return tutorial coupon

## Require:
- Python 3

### Python Module :

- BeautifulSoup 4

Debian install : 
```bash
sudo apt-get install python3-bs4
```
Or PIP install :
```bash
pip3 install beautifulsoup4
```

- Requests

Debian install : 
```bash
sudo apt-get install python3-requests
```
Or PIP install :
```bash
pip3 install requests
```

## Run :
```bash
python3 crawler.py
```